# LivingWithLag

The idea of this project is to find a way to artificially increase the latency of controller and HMD movement in VR.  
In order to provide the delayed movement, I use an array in which the pose data is stored for X ticks before they are used. 

## Limitation
Currently I do not use pose predictions and I fetch the tracking data quite early, which increases the latency even without buffering the pose data for a certain number ticks.  
E.g.when using an HTC Vive, without buffering the pose data, there will still be a latency of about 45 ms. 







