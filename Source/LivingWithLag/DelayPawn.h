// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "openvr.h"
#include "DelayPawn.generated.h"

UCLASS()
class LIVINGWITHLAG_API ADelayPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADelayPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Our connnection to the VR System
	vr::IVRSystem* VRSystem;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void InitSteamVR();

	UPROPERTY(BlueprintReadWrite)
	bool bPredict;
	UPROPERTY(BlueprintReadWrite)
	int nFrames;

	float fSecondsSinceLastVsync;
	float fDisplayFrequency;
	float fFrameDuration;
	float fVsyncToPhotons;
	float fPredictedSecondsFromNow;

	float w;
	float w4;
	float x;
	float y;
	float z;

	void PredictOnset(int n);




	UPROPERTY(BlueprintReadOnly)
	FVector LocationHMD;
	UPROPERTY(BlueprintReadOnly)
	FRotator RotationHMD;

	UPROPERTY(BlueprintReadOnly)
	FVector LocationCon1;
	UPROPERTY(BlueprintReadOnly)
	FRotator RotationCon1;

	UPROPERTY(BlueprintReadOnly)
	FVector LocationCon2;
	UPROPERTY(BlueprintReadOnly)
	FRotator RotationCon2;
	bool IsDeviceConnected;

private:
	uint32_t unTrackedDevicePoseArrayCount = vr::k_unMaxTrackedDeviceCount;
	vr::TrackedDevicePose_t trackedDevicePoseArray[vr::k_unMaxTrackedDeviceCount];
	void GetPose();



};
