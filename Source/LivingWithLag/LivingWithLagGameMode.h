// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LivingWithLagGameMode.generated.h"

UCLASS(minimalapi)
class ALivingWithLagGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALivingWithLagGameMode();
};



