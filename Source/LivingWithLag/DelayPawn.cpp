// Fill out your copyright notice in the Description page of Project Settings.


#include "DelayPawn.h"



// Sets default values
ADelayPawn::ADelayPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bPredict = false;
	nFrames = 1;
	
}

// Called when the game starts or when spawned
void ADelayPawn::BeginPlay()
{
	Super::BeginPlay();
	InitSteamVR();
	
}





void ADelayPawn::GetPose()
{
	if (bPredict == false) 
		{
		VRSystem->GetDeviceToAbsoluteTrackingPose(vr::TrackingUniverseOrigin::TrackingUniverseStanding, 0, trackedDevicePoseArray, vr::k_unMaxTrackedDeviceCount);
		}
	else
		{
		PredictOnset(nFrames);
		VRSystem->GetDeviceToAbsoluteTrackingPose(vr::TrackingUniverseOrigin::TrackingUniverseStanding, fPredictedSecondsFromNow, trackedDevicePoseArray, vr::k_unMaxTrackedDeviceCount);
		}


	for (int nDevice = 0; nDevice < 3; nDevice++)
	{
		vr::HmdMatrix34_t pose = trackedDevicePoseArray[nDevice].mDeviceToAbsoluteTracking;

		w = FGenericPlatformMath::Sqrt(1.0 + pose.m[0][0] + pose.m[1][1] + pose.m[2][2]) / 2.0;
		w4 = (4.0 * w);
		x = (pose.m[2][1] - pose.m[1][2]) / w4;
		y = (pose.m[0][2] - pose.m[2][0]) / w4;
		z = (pose.m[1][0] - pose.m[0][1]) / w4;

		if (nDevice == 0)
		{
			LocationHMD = (FVector(-pose.m[2][3], pose.m[0][3], pose.m[1][3]) * 100);
			RotationHMD = FRotator(FQuat(z, -x, -y, w).Rotator());
		}
		else if (nDevice > 0)
		{
			//VRSystem->GetControllerRoleForTrackedDeviceIndex(nDevice);
			auto trackedControllerRole = VRSystem->GetControllerRoleForTrackedDeviceIndex(nDevice);
			if (trackedControllerRole == vr::TrackedControllerRole_LeftHand)
			{
				LocationCon1 = (FVector(-pose.m[2][3], pose.m[0][3], pose.m[1][3]) * 100);
				RotationCon1 = FRotator(FQuat(z, -x, -y, w).Rotator());
			}
			else if (trackedControllerRole == vr::TrackedControllerRole_RightHand)
			{
				LocationCon2 = (FVector(-pose.m[2][3], pose.m[0][3], pose.m[1][3]) * 100);
				RotationCon2 = FRotator(FQuat(z, -x, -y, w).Rotator());
			}

		}


	}
}






// Called every frame
void ADelayPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	GetPose();
	//UE_LOG(LogTemp, Warning, TEXT(" %s  %s %s  %s  %s  %s "), *LocationHead.ToString(), *LocationHead.ToString(), *LocationCon1.ToString(), *LocationCon2.ToString(), *RotationCon1.ToString(), *RotationCon2.ToString());



}

// Called to bind functionality to input
void ADelayPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ADelayPawn::InitSteamVR()
{
	auto initError = vr::VRInitError_None;
	VRSystem = vr::VR_Init(&initError, vr::VRApplication_Other);

}




//Calculate seconds to photons. 
void ADelayPawn::PredictOnset(int n)
{

	VRSystem->GetTimeSinceLastVsync(&fSecondsSinceLastVsync, NULL);

	fDisplayFrequency = VRSystem->GetFloatTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_DisplayFrequency_Float);
	fFrameDuration = 1.f / fDisplayFrequency;


	fVsyncToPhotons = VRSystem->GetFloatTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_SecondsFromVsyncToPhotons_Float);

	fPredictedSecondsFromNow = (fFrameDuration - fSecondsSinceLastVsync + (fFrameDuration * n) + fVsyncToPhotons);



}


