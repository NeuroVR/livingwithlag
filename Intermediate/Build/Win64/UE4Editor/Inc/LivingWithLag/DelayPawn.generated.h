// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVINGWITHLAG_DelayPawn_generated_h
#error "DelayPawn.generated.h already included, missing '#pragma once' in DelayPawn.h"
#endif
#define LIVINGWITHLAG_DelayPawn_generated_h

#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_SPARSE_DATA
#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_RPC_WRAPPERS
#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADelayPawn(); \
	friend struct Z_Construct_UClass_ADelayPawn_Statics; \
public: \
	DECLARE_CLASS(ADelayPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ADelayPawn)


#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_INCLASS \
private: \
	static void StaticRegisterNativesADelayPawn(); \
	friend struct Z_Construct_UClass_ADelayPawn_Statics; \
public: \
	DECLARE_CLASS(ADelayPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ADelayPawn)


#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADelayPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADelayPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADelayPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADelayPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADelayPawn(ADelayPawn&&); \
	NO_API ADelayPawn(const ADelayPawn&); \
public:


#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADelayPawn(ADelayPawn&&); \
	NO_API ADelayPawn(const ADelayPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADelayPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADelayPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADelayPawn)


#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_PRIVATE_PROPERTY_OFFSET
#define livingwithlag_Source_LivingWithLag_DelayPawn_h_10_PROLOG
#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_RPC_WRAPPERS \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_INCLASS \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define livingwithlag_Source_LivingWithLag_DelayPawn_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_INCLASS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_DelayPawn_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVINGWITHLAG_API UClass* StaticClass<class ADelayPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID livingwithlag_Source_LivingWithLag_DelayPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
