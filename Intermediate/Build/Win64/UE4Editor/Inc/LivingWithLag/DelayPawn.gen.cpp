// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LivingWithLag/DelayPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDelayPawn() {}
// Cross Module References
	LIVINGWITHLAG_API UClass* Z_Construct_UClass_ADelayPawn_NoRegister();
	LIVINGWITHLAG_API UClass* Z_Construct_UClass_ADelayPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_LivingWithLag();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ADelayPawn::StaticRegisterNativesADelayPawn()
	{
	}
	UClass* Z_Construct_UClass_ADelayPawn_NoRegister()
	{
		return ADelayPawn::StaticClass();
	}
	struct Z_Construct_UClass_ADelayPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationCon2_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationCon2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationCon2_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationCon2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationCon1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationCon1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationCon1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationCon1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationHMD_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationHMD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationHMD_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationHMD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nFrames_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_nFrames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPredict_MetaData[];
#endif
		static void NewProp_bPredict_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPredict;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADelayPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_LivingWithLag,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "DelayPawn.h" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon2_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon2 = { "RotationCon2", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADelayPawn, RotationCon2), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon2_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon2 = { "LocationCon2", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADelayPawn, LocationCon2), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon1_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon1 = { "RotationCon1", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADelayPawn, RotationCon1), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon1_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon1 = { "LocationCon1", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADelayPawn, LocationCon1), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationHMD_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationHMD = { "RotationHMD", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADelayPawn, RotationHMD), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationHMD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationHMD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationHMD_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationHMD = { "LocationHMD", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADelayPawn, LocationHMD), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationHMD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationHMD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_nFrames_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_nFrames = { "nFrames", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADelayPawn, nFrames), METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_nFrames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_nFrames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADelayPawn_Statics::NewProp_bPredict_MetaData[] = {
		{ "Category", "DelayPawn" },
		{ "ModuleRelativePath", "DelayPawn.h" },
	};
#endif
	void Z_Construct_UClass_ADelayPawn_Statics::NewProp_bPredict_SetBit(void* Obj)
	{
		((ADelayPawn*)Obj)->bPredict = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADelayPawn_Statics::NewProp_bPredict = { "bPredict", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADelayPawn), &Z_Construct_UClass_ADelayPawn_Statics::NewProp_bPredict_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::NewProp_bPredict_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::NewProp_bPredict_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADelayPawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationCon1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationCon1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_RotationHMD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_LocationHMD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_nFrames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADelayPawn_Statics::NewProp_bPredict,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADelayPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADelayPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADelayPawn_Statics::ClassParams = {
		&ADelayPawn::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADelayPawn_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADelayPawn_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADelayPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADelayPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADelayPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADelayPawn, 1812505420);
	template<> LIVINGWITHLAG_API UClass* StaticClass<ADelayPawn>()
	{
		return ADelayPawn::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADelayPawn(Z_Construct_UClass_ADelayPawn, &ADelayPawn::StaticClass, TEXT("/Script/LivingWithLag"), TEXT("ADelayPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADelayPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
