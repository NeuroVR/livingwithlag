// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef LIVINGWITHLAG_LivingWithLagProjectile_generated_h
#error "LivingWithLagProjectile.generated.h already included, missing '#pragma once' in LivingWithLagProjectile.h"
#endif
#define LIVINGWITHLAG_LivingWithLagProjectile_generated_h

#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_SPARSE_DATA
#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALivingWithLagProjectile(); \
	friend struct Z_Construct_UClass_ALivingWithLagProjectile_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ALivingWithLagProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesALivingWithLagProjectile(); \
	friend struct Z_Construct_UClass_ALivingWithLagProjectile_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ALivingWithLagProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALivingWithLagProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALivingWithLagProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALivingWithLagProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALivingWithLagProjectile(ALivingWithLagProjectile&&); \
	NO_API ALivingWithLagProjectile(const ALivingWithLagProjectile&); \
public:


#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALivingWithLagProjectile(ALivingWithLagProjectile&&); \
	NO_API ALivingWithLagProjectile(const ALivingWithLagProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALivingWithLagProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALivingWithLagProjectile)


#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ALivingWithLagProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ALivingWithLagProjectile, ProjectileMovement); }


#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_9_PROLOG
#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_RPC_WRAPPERS \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_INCLASS \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_INCLASS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVINGWITHLAG_API UClass* StaticClass<class ALivingWithLagProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID livingwithlag_Source_LivingWithLag_LivingWithLagProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
