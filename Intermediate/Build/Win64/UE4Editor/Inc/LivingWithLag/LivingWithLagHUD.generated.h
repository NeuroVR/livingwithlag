// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVINGWITHLAG_LivingWithLagHUD_generated_h
#error "LivingWithLagHUD.generated.h already included, missing '#pragma once' in LivingWithLagHUD.h"
#endif
#define LIVINGWITHLAG_LivingWithLagHUD_generated_h

#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_SPARSE_DATA
#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_RPC_WRAPPERS
#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALivingWithLagHUD(); \
	friend struct Z_Construct_UClass_ALivingWithLagHUD_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ALivingWithLagHUD)


#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesALivingWithLagHUD(); \
	friend struct Z_Construct_UClass_ALivingWithLagHUD_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ALivingWithLagHUD)


#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALivingWithLagHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALivingWithLagHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALivingWithLagHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALivingWithLagHUD(ALivingWithLagHUD&&); \
	NO_API ALivingWithLagHUD(const ALivingWithLagHUD&); \
public:


#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALivingWithLagHUD(ALivingWithLagHUD&&); \
	NO_API ALivingWithLagHUD(const ALivingWithLagHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALivingWithLagHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALivingWithLagHUD)


#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_9_PROLOG
#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_RPC_WRAPPERS \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_INCLASS \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_INCLASS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVINGWITHLAG_API UClass* StaticClass<class ALivingWithLagHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID livingwithlag_Source_LivingWithLag_LivingWithLagHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
