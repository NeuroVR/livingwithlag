// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LivingWithLag/LivingWithLagGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLivingWithLagGameMode() {}
// Cross Module References
	LIVINGWITHLAG_API UClass* Z_Construct_UClass_ALivingWithLagGameMode_NoRegister();
	LIVINGWITHLAG_API UClass* Z_Construct_UClass_ALivingWithLagGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_LivingWithLag();
// End Cross Module References
	void ALivingWithLagGameMode::StaticRegisterNativesALivingWithLagGameMode()
	{
	}
	UClass* Z_Construct_UClass_ALivingWithLagGameMode_NoRegister()
	{
		return ALivingWithLagGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ALivingWithLagGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALivingWithLagGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_LivingWithLag,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALivingWithLagGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "LivingWithLagGameMode.h" },
		{ "ModuleRelativePath", "LivingWithLagGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALivingWithLagGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALivingWithLagGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALivingWithLagGameMode_Statics::ClassParams = {
		&ALivingWithLagGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ALivingWithLagGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALivingWithLagGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALivingWithLagGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALivingWithLagGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALivingWithLagGameMode, 1926901121);
	template<> LIVINGWITHLAG_API UClass* StaticClass<ALivingWithLagGameMode>()
	{
		return ALivingWithLagGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALivingWithLagGameMode(Z_Construct_UClass_ALivingWithLagGameMode, &ALivingWithLagGameMode::StaticClass, TEXT("/Script/LivingWithLag"), TEXT("ALivingWithLagGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALivingWithLagGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
