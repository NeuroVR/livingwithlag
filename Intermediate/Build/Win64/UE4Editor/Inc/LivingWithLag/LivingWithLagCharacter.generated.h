// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVINGWITHLAG_LivingWithLagCharacter_generated_h
#error "LivingWithLagCharacter.generated.h already included, missing '#pragma once' in LivingWithLagCharacter.h"
#endif
#define LIVINGWITHLAG_LivingWithLagCharacter_generated_h

#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_SPARSE_DATA
#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_RPC_WRAPPERS
#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALivingWithLagCharacter(); \
	friend struct Z_Construct_UClass_ALivingWithLagCharacter_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ALivingWithLagCharacter)


#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesALivingWithLagCharacter(); \
	friend struct Z_Construct_UClass_ALivingWithLagCharacter_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), NO_API) \
	DECLARE_SERIALIZER(ALivingWithLagCharacter)


#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALivingWithLagCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALivingWithLagCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALivingWithLagCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALivingWithLagCharacter(ALivingWithLagCharacter&&); \
	NO_API ALivingWithLagCharacter(const ALivingWithLagCharacter&); \
public:


#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALivingWithLagCharacter(ALivingWithLagCharacter&&); \
	NO_API ALivingWithLagCharacter(const ALivingWithLagCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALivingWithLagCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALivingWithLagCharacter)


#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ALivingWithLagCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ALivingWithLagCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ALivingWithLagCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ALivingWithLagCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ALivingWithLagCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ALivingWithLagCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ALivingWithLagCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ALivingWithLagCharacter, L_MotionController); }


#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_11_PROLOG
#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_RPC_WRAPPERS \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_INCLASS \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_INCLASS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVINGWITHLAG_API UClass* StaticClass<class ALivingWithLagCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID livingwithlag_Source_LivingWithLag_LivingWithLagCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
