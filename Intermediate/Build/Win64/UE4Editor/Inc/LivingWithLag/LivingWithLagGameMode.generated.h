// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVINGWITHLAG_LivingWithLagGameMode_generated_h
#error "LivingWithLagGameMode.generated.h already included, missing '#pragma once' in LivingWithLagGameMode.h"
#endif
#define LIVINGWITHLAG_LivingWithLagGameMode_generated_h

#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_SPARSE_DATA
#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_RPC_WRAPPERS
#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALivingWithLagGameMode(); \
	friend struct Z_Construct_UClass_ALivingWithLagGameMode_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), LIVINGWITHLAG_API) \
	DECLARE_SERIALIZER(ALivingWithLagGameMode)


#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesALivingWithLagGameMode(); \
	friend struct Z_Construct_UClass_ALivingWithLagGameMode_Statics; \
public: \
	DECLARE_CLASS(ALivingWithLagGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LivingWithLag"), LIVINGWITHLAG_API) \
	DECLARE_SERIALIZER(ALivingWithLagGameMode)


#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LIVINGWITHLAG_API ALivingWithLagGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALivingWithLagGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LIVINGWITHLAG_API, ALivingWithLagGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LIVINGWITHLAG_API ALivingWithLagGameMode(ALivingWithLagGameMode&&); \
	LIVINGWITHLAG_API ALivingWithLagGameMode(const ALivingWithLagGameMode&); \
public:


#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LIVINGWITHLAG_API ALivingWithLagGameMode(ALivingWithLagGameMode&&); \
	LIVINGWITHLAG_API ALivingWithLagGameMode(const ALivingWithLagGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LIVINGWITHLAG_API, ALivingWithLagGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALivingWithLagGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALivingWithLagGameMode)


#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_9_PROLOG
#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_RPC_WRAPPERS \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_INCLASS \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_SPARSE_DATA \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_INCLASS_NO_PURE_DECLS \
	livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVINGWITHLAG_API UClass* StaticClass<class ALivingWithLagGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID livingwithlag_Source_LivingWithLag_LivingWithLagGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
